//
//  Menu.swift
//  Academy's Cafe
//
//  Created by Almas Sharfina on 03/06/20.
//  Copyright © 2020 R. Kukuh. All rights reserved.
//

import Foundation

class Menu {
    var  menuID : String = ""
    var namaMenu : String = ""
    
    init(menuID : String, menuName: String){
        self.menuID = menuID
        self.namaMenu = menuName
    }
}
